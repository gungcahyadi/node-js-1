const express = require('express');
const app = express();
const port = 1000;
app.set('port', process.env.port || port);

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

/* 
- View Home
*/
app.get("/", (request, response) => {
	response.render('index.ejs')
});
app.get("/daftar", (request, response) => {
	response.render('pendaftaran.ejs')
});

app.listen(port, () => console.log('Server Running on port : ' + port));
